<!--
 * @Author: tao
 * @Date: 2020-09-21 17:11:26
 * @LastEditTime: 2020-10-09 11:00:35
 * @LastEditors: Please set LastEditors
 * @Description: 项目介绍
 * @FilePath: tao-icon\README.en.md
-->

# tao-icon

#### 介绍

字体图标和 svg 图标库，基于 阿里图标库和 fontawesome，依赖 element-ui 和 vue 脚手架构建的基础项目

#### 使用说明

```html
npm install tao-icon --save

import taoIcon from 'tao-icon'
import "tao-icon/lib/taoIcon.css";
Vue.use(taoIcon)
```

图标显示组件:

```html
<tao-icon value="fa pie-chart 32" />1
<tao-icon value="iconfont faqikaohe 32" />2
<tao-icon value="svg-icon iconchuangjianzhibiao 64" />3
```

选择图标组件

```html
<tao-icon-select v-model="icon" /> icon:'svg-icon tao-todo 124'
```

##### 组件说明

项目集成了 fontawesome 和阿里云项目图标和自定义 svg 图标库,自定义扩展 svg 图标可以 copy 项目在 src\assets\svg-icons\icons 下面加入你自己的 svg 图标即可
效果展示：
![](https://gitee.com/upholdjx/timg/raw/master/img/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20201009105105.png)
![](https://gitee.com/upholdjx/timg/raw/master/img/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20201009105318.png)
![](https://gitee.com/upholdjx/timg/raw/master/img/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20201009110013.png)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
