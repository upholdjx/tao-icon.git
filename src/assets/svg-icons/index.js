/*
 * @Author: your name
 * @Date: 2020-09-21 15:05:00
 * @LastEditTime: 2020-09-21 15:45:45
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \AK-FRONT-5.0e:\demo\t-icon\src\assets\svg-icons\index.js
 */
import Vue from 'vue'

const requireAll = requireContext => requireContext.keys().map(requireContext)
const req = require.context('./icons', false, /\.svg$/)
const iconMap = requireAll(req)

Vue.prototype.$IconSvg = iconMap.map(e => e.default.id.slice(4))
