/*
 * @Author: your name
 * @Date: 2020-09-21 17:15:28
 * @LastEditTime: 2020-09-21 17:17:50
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \AK-FRONT-5.0e:\5.0_apld\tao-icon\babel.config.js
 */
module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset',
    ["@babel/preset-env", { modules: false }]
  ],
  plugins: [
    [
      "component",
      {
        libraryName: "element-ui",
        styleLibraryName: "theme-chalk"
      }
    ]
  ]
}
